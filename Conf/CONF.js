require("babel-register")({
    presets: [ 'es2015' ]
});

var params = require('../PF/Parameters.js');

exports.config = {

      // Capabilities to be passed to the webdriver instance.
      capabilities: {
        'browserName': 'chrome'
      },
    directConnect: true,
  // seleniumAddress: 'http://localhost:4444/wd/hub',
  // seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['TST.js'],
  
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
  },

  params: {
    login: {
      email: 'email_test',
      password: 'password_test'
    }
  },

    beforeLaunch: () => {
          //surround with try-catch to avoid async calls of readParameters()
        try {
            params.readParameters();
        }catch(error){
            console.log(error);
        }
    },


   onCleanUp: (exitCode) => {
		if (exitCode == 0 ){
			console.log('PASSED');
			params.setResult("success", "Service was ended with PASS status", null, null, null);
		} else if (exitCode == 1) {
			console.log('FAILED');
            params.setResult("failure", "Service was ended with FAILED status: details are available in Technical information", null, null, null);
        } else {
		    console.log('UNKNOWN');
		}
       console.log(exitCode);
    },
};

