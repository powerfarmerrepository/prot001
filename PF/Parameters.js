var fs = require('fs');
var path = require('canonical-path');
var collection = require('./collections.js')

module.exports = {

    //z pomienieciem zagniezdzonych tablic w json!!!
    readParameters() {
        var parameters = ["options.json", "arguments.json"];
        parameters.forEach(function (entry) {
            fs.stat(path.resolve(process.cwd(), './', entry), function (err, stat) {
                if (err == null) {
                    var json = fs.readFileSync(path.resolve(process.cwd(), './', entry));
                    var result = JSON.parse(json);
                    for (var prop in result) {
                        if (!(result[prop] instanceof Object)) {
                            collection.IN.set(prop, result[prop]);
                        }
                    }
                } else if (err.code == 'ENOENT') {
                    console.log("File does not exists");
                } else {
                    console.log('Some other error: ', err.code);
                }
            });
        });
        return;
    },

    //tworzenie wyjsciowego jsona
    setResult(status, resultMessage, stackTrace, causedBy, screenshot) {
        var result = new Map();
        var outputFile = getDirectoryPath();
        collection.OUT.set("status", status);
        collection.OUT.set("result", resultMessage);
        result.set("stackTrace", stackTrace);
        result.set("causedBy", causedBy);
        result.set("screenshot", screenshot);
        collection.OUT.set("errors", result);

        //save to file
        let obj = Object.create(null);
        for (let [k, v] of collection.OUT) {
            obj[k] = v;
        }
        fs.appendFileSync(outputFile, JSON.stringify(obj));
    },

    getParam(parameter){
        return collection.IN.get(parameter);
    }
}

function getDirectoryPath() {
       var pathToCurrentDirectory = process.cwd();
       var dirPath;
	   console.log(pathToCurrentDirectory);
       if (pathToCurrentDirectory.includes("WORKER")) {
           dirPath = path.resolve(process.cwd(), './workspace/result', "result.json");       }
       else {
           dirPath = path.resolve(process.cwd(), './', "result.json");
       }
       return dirPath;
   }